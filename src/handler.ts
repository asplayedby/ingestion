import { APIGatewayEvent, APIGatewayProxyHandler, APIGatewayProxyResult, Context } from "aws-lambda";
import { KMS } from "aws-sdk";

import "reflect-metadata";
import { ConnectionOptions, createConnection } from "typeorm";

import { SpotifyService } from "./service/SpotifyService";

import { Ingestor } from "./Ingestor";

let decrypted = false;
let spotifySecret: string;

const handler: APIGatewayProxyHandler = async (event: APIGatewayEvent, context: Context) => {
    const response: APIGatewayProxyResult = {
        body: "",
        headers: {
            "Access-Control-Allow-Origin": "*",
        },
        isBase64Encoded: false,
        statusCode: 200,
    };
    try {
        console.log(`Event recieved: ${JSON.stringify(event)}`);
        if (!decrypted) {
            context.callbackWaitsForEmptyEventLoop = false;
            // Decrypt code should run once and variables stored outside of the function
            // handler so that these are decrypted once per container
            const kms = new KMS();
            const encrypted = process.env.DATABASE_PASSWORD;
            console.log("Attempting to decrypt DATABASE_PASSWORD");
            const decryptedValue = await kms.decrypt({ CiphertextBlob: new Buffer(encrypted, "base64") }).promise();
            console.log("Successfully decrypted DATABASE_PASSWORD");
            const password = decryptedValue.Plaintext.toString();
            const connectionOptions: ConnectionOptions = {
                database: process.env.DATABASE,
                entities: [
                    process.env.LAMBDA_TASK_ROOT + "/entity/*.js",
                ],
                host: process.env.DATABASE_HOST,
                logging: process.env.DATABASE_LOGGING === "true",
                password,
                port: parseInt(process.env.DATABASE_PORT, 10),
                synchronize: true,
                type: "postgres",
                username: process.env.DATABASE_USERNAME,
            };
            console.log("Attempting to connect to database");
            await createConnection(connectionOptions);
            console.log("Successfully connected to database");
            const encrypedSecret = process.env.SPOTIFY_CLIENT_SECRET;
            console.log("Attempting to decrypt spotify_secret");
            const decryptedSec = await kms.decrypt({ CiphertextBlob: new Buffer(encrypedSecret, "base64") }).promise();
            console.log("Successfully decrypted spotify_secret");
            spotifySecret = decryptedSec.Plaintext.toString();
            decrypted = true;
        }
        const { code, redirect } = event.queryStringParameters;
        const spotifyService = new SpotifyService(process.env.SPOTIFY_CLIENT_ID, spotifySecret, code, redirect);
        const ingestor = new Ingestor(spotifyService);
        const responseBody = await ingestor.ingest(JSON.parse(event.body));
        response.body = JSON.stringify(responseBody);
        response.statusCode = 200;
    } catch (e) {
        console.error(e);
        response.body = JSON.stringify(e);
        response.statusCode = 500;
    }
    return response;
};

exports.handler = handler;
