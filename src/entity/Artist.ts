import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Artist extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column("text", { array: true, nullable: true })
    public spotifyIdCandidates: string[];

    @Column({ nullable: true })
    public spotifyId: string;

    @Column({ nullable: true })
    public spotifyPlaylistId: string;

}
