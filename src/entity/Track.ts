import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Track extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public artist: string;

    @Column({ nullable: true })
    public spotifyId: string;

    @Column("text", { array: true, nullable: true })
    public spotifyIdCandidates: string[];

}
