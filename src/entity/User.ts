import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class User extends BaseEntity {

    @PrimaryColumn()
    public id: string;

    @Column()
    public refreshToken: string;

    @Column()
    public accessToken: string;

    @Column()
    public code: string;

    @Column("timestamp")
    public lastAccess: Date;

}
