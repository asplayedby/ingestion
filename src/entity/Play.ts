import { BaseEntity, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Set } from "./Set";
import { Track } from "./Track";

@Entity()
export class Play extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @OneToOne(() => Track)
    @JoinColumn()
    public track: Track;

    @OneToOne(() => Set)
    @JoinColumn()
    public set: Set;

}
