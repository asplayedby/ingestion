import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Artist } from "./Artist";

@Entity()
export class Set extends BaseEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column("timestamp")
    public playedAt: Date;

    @OneToOne(() => Artist)
    @JoinColumn()
    public playedBy: Artist;

    @Column({ nullable: true })
    public spotifyPlaylistId: string;

}
