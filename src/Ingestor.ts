import { Promise as bluebird } from "bluebird";
import "reflect-metadata";

import { Artist } from "./entity/Artist";
import { Play } from "./entity/Play";
import { Set } from "./entity/Set";
import { Track } from "./entity/Track";
import { SpotifyService } from "./service/SpotifyService";

interface ITempTrack {
    artistName: string;
    trackName: string;
    labelName?: string;
    featureName?: string;
}

export class Ingestor {

    constructor(private spotifyService: SpotifyService) { }

    public async ingest(body) {
        console.log(`Attempting to ingest: ${JSON.stringify(body)}`);
        const playedAt = new Date(body.playedAt);
        const playedBy = await this.ingestArtist(body.playedBy);
        const set = await this.ingestSet(playedAt, playedBy, body.name);

        const trackRegex = /(.*?) - (.*?)(\(feat\..*?\))?\s*(\[.*?\])?\s?\n/g;
        let match = trackRegex.exec(body.setList);
        const rawTracks: ITempTrack[] = [];
        // create set
        while (match) {
            const [, artistName, trackName, featureName, labelName] = match;
            rawTracks.push({
                artistName,
                featureName,
                labelName,
                trackName,
            });
            match = trackRegex.exec(body.setList);
        }

        const tracks = await bluebird.map(rawTracks, async (rawTrack) => {
            const artist = await this.ingestArtist(rawTrack.artistName);
            const track = await this.ingestTrack(artist, rawTrack.trackName);
            await this.ingestPlay(set, track);
            return track;
        }, { concurrency: parseInt(process.env.TRACK_CONCURRENCY, 10) });

        const foundTracksIds = tracks.filter((track) => track.spotifyId != null).map((track) => track.spotifyId);
        await this.spotifyService.addTracksToPlaylist(foundTracksIds, set.spotifyPlaylistId);
        const result = {
            set,
            tracks,
        };
        return result;
    }

    private async ingestSet(playedAt: Date, playedBy: Artist, name: string) {
        let set = await Set.findOne({
            where: {
                playedAt: playedAt.toISOString(),
                playedBy: playedBy.id,
            },
        });
        if (!set) {
            set = new Set();
            set.playedAt = playedAt;
            set.playedBy = playedBy;
            set.name = name;
            const playlist = await this.spotifyService.createPlaylist(set.name);
            set.spotifyPlaylistId = playlist.id;
            console.log(`Attempting to save Set: ${JSON.stringify(set)}`);
            set = await set.save();
        }
        return set;
    }

    private async ingestArtist(name: string) {
        let artist = await Artist.findOne({ name });
        if (!artist) {
            artist = new Artist();
            artist.name = name;
            artist.spotifyIdCandidates = await this.spotifyService.getArtistCandidates(name);
            artist.spotifyId = artist.spotifyIdCandidates[0];
            console.log(`Attempting to save Artist: ${JSON.stringify(artist)}`);
            artist = await artist.save();
        }
        return artist;
    }

    private async ingestTrack(artist: Artist, name: string) {
        let track = await Track.findOne({
            where: {
                artist: artist.id,
                name,
            },
        });
        if (!track) {
            track = new Track();
            track.name = name;
            track.artist = artist.name;
            track.spotifyIdCandidates = await this.spotifyService.getTrackCandidates(artist.name, name);
            track.spotifyId = track.spotifyIdCandidates[0];
            console.log(`Attempting to save Track: ${JSON.stringify(track)}`);
            track = await track.save();
        }
        return track;
    }

    private async ingestPlay(set: Set, track: Track) {
        let play = await Play.findOne({
            where: {
                set: set.id,
                track: track.id,
            },
        });
        if (!play) {
            play = new Play();
            play.set = set;
            play.track = track;
            console.log(`Attempting to save Play: ${JSON.stringify(play)}`);
            play = await play.save();
        }
    }
}
