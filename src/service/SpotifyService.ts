import SpotifyWebApi, { AuthorizationGrant, Playlist } from "spotify-web-api-node";

import { Promise as bluebird } from "bluebird";
import { User } from "../entity/User";

export class SpotifyService {

    private spotify: SpotifyWebApi;
    private tokenExpiration: Date;
    private code: string;
    private username: string;

    constructor(clientId: string, clientSecret: string, code: string, redirectUri: string) {
        this.code = code;
        console.log(`Auth code: ${this.code}`);
        const credentials = {
            clientId, clientSecret, redirectUri,
        };
        console.log(`Credentials: ${JSON.stringify(credentials)}`);
        this.spotify = new SpotifyWebApi(credentials);
    }

    public async getArtistCandidates(name: string): Promise<string[]> {
        await this.authenticate();
        console.log(`Searching for ${name}`);
        const searchResults = await this.spotify.searchArtists(name);
        console.log(`Successfully executed query: ${name}, Results: ${JSON.stringify(searchResults.body)}`);
        const artists = searchResults.body.artists.items;
        const artistIds = artists.map((artist) => artist.id);
        console.log(`Found ${artists.length} artists`);
        console.log(`ArtistIds: ${artistIds}`);
        return artistIds;
    }

    public async getTrackCandidates(artistName: string, trackName: string): Promise<string[]> {
        await this.authenticate();
        const query = `artist:${artistName} track:${trackName}`;
        console.log(`Searching for ${query}`);
        const searchResults = await this.spotify.searchTracks(query);
        console.log(`Successfully executed query: ${query}, Results: ${JSON.stringify(searchResults.body)}`);
        const tracks = searchResults.body.tracks.items;
        let trackIds = tracks.map((track) => track.id);
        const multipleArtists = artistName.split(" & ").length > 1;
        if (tracks.length === 0 && multipleArtists) {
            const listOfLists = await bluebird.map(artistName.split("&"), async (artist) => {
                return await this.getTrackCandidates(artist, trackName);
            });
            trackIds = [].concat(...listOfLists);
        }
        console.log(`Found ${tracks.length} tracks`);
        console.log(`TrackIds: ${trackIds}`);
        return trackIds;
    }

    public async createPlaylist(playlistName: string): Promise<Playlist> {
        await this.authenticate();
        console.log(`Attepting to create playlist ${playlistName} for user ${this.username}`);
        const playlist = await this.spotify.createPlaylist(this.username, playlistName);
        console.log(`Successfully created playlist ${playlistName}`);
        return playlist.body;
    }

    public async addTracksToPlaylist(trackIds: string[], playlistId: string): Promise<void> {
        await this.authenticate();
        console.log(`Attempting to add ${trackIds} to ${playlistId}`);
        const trackUris = trackIds.map((trackId) => `spotify:track:${trackId}`);
        await this.spotify.addTracksToPlaylist(this.username, playlistId, trackUris);
        console.log(`Successfully added ${trackIds} to ${playlistId}`);
    }

    private async authenticate() {
        if (!this.tokenExpiration) {
            console.log("No access token exists");
            await this.getToken();
            return;
        }

        const now = new Date();
        const tokenExpired = this.tokenExpiration && now >= this.tokenExpiration;
        if (tokenExpired) {
            console.log(`It is now ${now}, token expired at ${this.tokenExpiration}`);
            await this.refreshToken();
            return;
        }
    }

    private async getToken() {
        // Retrieve an access token.
        console.log("Attempting to get new access token");
        const user = await User.findOne({
            where: {
                code: this.code,
            },
        });
        let data: SpotifyWebApi.Response<AuthorizationGrant>;
        if (!user) {
            data = await this.spotify.authorizationCodeGrant(this.code);
        } else {
            console.log("Found user with this code, attempting to refresh");
            this.spotify.setAccessToken(user.accessToken);
            this.spotify.setRefreshToken(user.refreshToken);
            data = await this.spotify.refreshAccessToken();
        }
        await this.handleAuthResponse(data.body);
        console.log("Successfully retrieved access token");
    }

    private async refreshToken() {
        console.log("Attempting to refresh access token");
        const data = await this.spotify.refreshAccessToken();
        console.log("Successfully retrieved access token");
        await this.handleAuthResponse(data.body);
    }

    private async handleAuthResponse(grant: AuthorizationGrant) {
        console.log(`Processing authorization grant ${JSON.stringify(grant)}`);
        this.spotify.setAccessToken(grant.access_token);
        this.spotify.setRefreshToken(grant.refresh_token);
        const expiresMs = new Date().getTime() + grant.expires_in * 1000;
        const expires = new Date();
        expires.setTime(expiresMs);
        this.tokenExpiration = expires;

        console.log("Attepting to 'getMe'");
        console.log(`Using credentials ${JSON.stringify(this.spotify.getCredentials())}`);
        console.log(`Using access token ${JSON.stringify(this.spotify.getAccessToken())}`);
        const meResponse = await this.spotify.getMe();
        // const meResponse = {
        //     body: {
        //         id: "burgerboy9n",
        //     },
        // };
        console.log(`Successfully retrieved me: ${JSON.stringify(meResponse.body)}`);
        const username = meResponse.body.id;
        this.username = username;
        let user = await User.findOne({
            where: {
                id: username,
            },
        });
        if (!user) {
            user = new User();
            user.id = username;
        }
        user.code = this.code;
        user.refreshToken = grant.refresh_token;
        user.accessToken = grant.access_token;
        user.lastAccess = new Date();
        console.log(`Attepting to save user: ${JSON.stringify(user)}`);
        await user.save();
    }
}
